package in.cioc.syrow.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import in.cioc.syrow.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent1 = new Intent(this,ChatRoomActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("email","kishan@gmail.com");
        bundle.putString("name","kishan");
        bundle.putString("CompanyId","1");
        bundle.putString("phoneNumber","21121");
        intent1.putExtras(bundle);
        startActivity(intent1);

        // startActivity(new Intent(this, ChatRoomActivity.class));
    }
    public void nextPage(View v){

        Intent intent1 = new Intent(this,ChatRoomActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("email","kishan@gmail.com");
        bundle.putString("name","kishan");
        bundle.putString("phoneNumber","21121");
        bundle.putString("CompanyId","1");
        intent1.putExtras(bundle);
        startActivity(intent1);
    }
}
