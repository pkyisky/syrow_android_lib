package in.cioc.syrow.miscellaneous;

public interface ImageDownloadListener {
    void onUpdate(int progress);
}
